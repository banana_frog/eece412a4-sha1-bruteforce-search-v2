/*
 *  wrapper.cpp
 *  e412a4-1
 */

#include "wrapper.h"

#include "bfthread.h"
#include <pthread.h>

Wrapper::Wrapper(std::string hash, std::string chars, int pwdlength, std::string salt):
	target(hash), chars(chars), plength(pwdlength) {
	
	success = false;
		
	//create threads
    
    //use default attributes
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	
	//Create a bfthread for every character in chars
	// Each bfthread t then works on a password 1 character shorter than the real password, 
	// with salt = old salt + chars[t]
	pthread_t tids[chars.length()];
	
	WrapStruct wraps[chars.length()];
	
	for(int i=0; i<chars.length(); i++) {
		wraps[i].wrapper = this;
		wraps[i].salt = salt + chars[i];
		pthread_create(&tids[i], &attr, &Wrapper::getpwd, &wraps[i]);
	}
	
	//wait for threads to terminate
	for(int i=0; i<chars.length(); i++)
		pthread_join(tids[i], NULL);
	
}
	

std::string Wrapper::getPassword() {
	if(!success) 
		return "not found"; 
	return password;
}

void* Wrapper::getpwd(void* param) {
	
	WrapStruct* ws = (WrapStruct*)(param);
	
	BFthread s(&(ws->wrapper->success), ws->wrapper->target, ws->wrapper->chars, ws->wrapper->plength-1, ws->salt);
	
	// probability of collision is extremely low, so only 
	// one thread will enter this block
	if(s.hasPassword()) 
		ws->wrapper->password = "" + s.getSalt(-1) + s.getPassword();
	
	pthread_exit(NULL);
}
