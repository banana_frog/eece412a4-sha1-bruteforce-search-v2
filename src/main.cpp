#include <iostream>
#include "wrapper.h"

int main (int argc, char * const argv[]) {
	
	if(argc < 4 || argc > 5) {
		std::cout << "Usage: [program name] [hash] [acceptable characters] [password length] [salt] \n";
		return 0;
	}
	
	std::string salt = "";
	
	if(argc == 5) {
		salt = argv[4];
		std::cout << "salt: |" << argv[4] << "|\n";
	} 
	
	Wrapper w(argv[1], argv[2], atoi(argv[3]), salt);
	
    std::cout << "target hash: |" << argv[1] << "|\ncharacters: |" << argv[2] <<
	"|\npassword: |" + w.getPassword() + "|\n";
    return 0;
}
