/*
 *  bfthread.h
 *  e412a4-1
 */


#ifndef BFTHREAD_H
#define BFTHREAD_H

#include <string>

#define FAILED 0
#define SUCCEEDED 1
#define CANCELLED 2

class BFthread {
	
private:
	std::string salt;
	std::string target;
	std::string password;
	std::string chars;
	int plength;
	int* pmap;
	int success;
	bool* stopSignal;
	
	void makePassword();
	bool increment();
	bool compareToHash();
	void build();
	void cleanup();
	
public:
	
	BFthread(bool* stopsignal, std::string hash, std::string chars, int pwdlength, std::string salt="");
	
	std::string getPassword();
	std::string getSalt(int n=0);
	bool hasPassword();
	
};

#endif
