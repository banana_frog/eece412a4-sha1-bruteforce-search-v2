/*
 *  wrapper.h
 *  e412a4-1
 */

#ifndef WRAPPER_H
#define WRAPPER_H

#include <string>

class Wrapper {
	
private:
	
	bool success;
	std::string password;
	
	std::string target;
	std::string chars; 
	int plength;
	
	static void* getpwd(void* param);
	
public:
	
	Wrapper(std::string hash, std::string chars, int pwdlength, std::string salt="");
	
	std::string getPassword();

};

typedef struct WrapStruct {
	Wrapper* wrapper;
	std::string salt;
};


#endif
