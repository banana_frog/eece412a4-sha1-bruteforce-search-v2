/*
 *  bfthread.cpp
 *  e412a4-1
 */

#include "bfthread.h"
#include "sha1.h"
#include <cstring>
#include <algorithm>

void BFthread::build() {

	success = SUCCEEDED;
	pmap = new int[plength];
	std::fill_n(pmap, plength, 0);
	
	while(!compareToHash()) {
		if(stopSignal != NULL && *stopSignal) {
			success = CANCELLED;
			break;
		}
		if(!increment()) {
			success = FAILED;
			break;
		}
	}
	
	if(stopSignal != NULL && success == SUCCEEDED)
		*stopSignal = true;
	
	cleanup();
}


BFthread::BFthread(bool* stopsignal, std::string hash, std::string chars, int pwdlength, std::string salt):
stopSignal(stopsignal), salt(salt), target(hash), chars(chars), plength(pwdlength) { 
	std::transform(target.begin(), target.end(), target.begin(), ::tolower);	
	build();
}

std::string BFthread::getPassword() {
	if(success == FAILED) 
		return "not found";
	if(success == CANCELLED)
		return "not found: search cancelled";
	return password;
}

std::string BFthread::getSalt(int n) {
	if(std::abs(n) > salt.length())
		return salt;
	
	if(n < 0)
		return salt.substr(salt.length()+n);
	
	return salt.substr(n);
}

void BFthread::makePassword() {
	password = "";
	for(int i=0; i<plength; i++) {
		password += chars[pmap[i]];
	}
}	


bool BFthread::increment() {
	int i = 0;
	
	while(i < plength && pmap[i] == chars.length()) {
		pmap[i] = 0;
		i++;
	}
	
	if(i == plength) return false;
	
	pmap[i] = pmap[i] + 1;
	return true;
}

bool BFthread::compareToHash() {
	makePassword();
	std::string toHash = salt + password;
	
	unsigned char temphash[20];
	char hexstring[41];
	sha1::calc(toHash.c_str(),toHash.length(), temphash); 
	sha1::toHexString(temphash, hexstring);
	
	return std::strncmp(hexstring, target.c_str(), target.length()) == 0;
}

void BFthread::cleanup() {
	if(pmap != NULL) {
		free(pmap);
		pmap = NULL;
	}

	stopSignal = NULL;
}

bool BFthread::hasPassword() {
	return success == SUCCEEDED;
}

